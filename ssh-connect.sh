#!/bin/sh

# fallback to login shell if remote user did not provide command
if [ -z "$SSH_ORIGINAL_COMMAND" ]; then
	USHELL=`getent passwd $USER | sed -e 's/.*://'`
	#USHELL=/usr/bin/mysecureshell
	SSH_ORIGINAL_COMMAND="${USHELL:=/bin/sh} -l"
fi

# fetch remote address
read REMOTE_ADDR REMOTE_PORT LOCAL_ADDR LOCAL_PORT <<EOF
$SSH_CONNECTION
EOF

# request ssh service
SERVICE=ssh
if [ "$SSH_ORIGINAL_COMMAND" = /usr/lib/ssh/sftp-server ]; then
	# sftp ...
	SERVICE=file
fi
# issue 2FA
if ! 2fad request "$SERVICE" "$REMOTE_ADDR"; then
	exit 1
fi

# repeat sshrc, since this shell reverted umask :-(
if [ -f ~/.ssh/rc ]; then
	. ~/.ssh/rc
elif [ -f /etc/ssh/sshrc ]; then
	. /etc/ssh/sshrc
fi

# run original command
exec $SSH_ORIGINAL_COMMAND
