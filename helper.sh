#!/bin/sh

read UID
read SVC
read IP
read SEED

echo "$ $0 $@ ($UID $SVC $IP $SEED)" >&2
sleep 5
TMPFILE="/tmp/`basename "$0"`-$$"
if ./2fad ls - - - "$SEED" > "$TMPFILE"; then
	read LSUID LSSVC LSIP LSSEED REST <"$TMPFILE"
fi
./2fad authorize "$UID" "$SVC" "$LSIP" "$SEED" ok 600
