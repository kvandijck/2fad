/* See LICENSE file for copyright and license details. */
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <locale.h>
#include <pwd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include "lib/libt.h"
#include "lib/libe.h"
#include "lib/liburi.h"

#define NAME "2fad"
const char *const progname = NAME;

/* program options */
static const char help_msg[] =
	NAME ": 2 Factor Authentication helper daemon\n"
	"usage:	" NAME " [OPTIONS ...] -s SCRIPT [ARGS ...]\n"
	"	" NAME " [OPTIONS ...] request SERVICE IP\n"
	"	" NAME " [OPTIONS ...] request [UID] SERVICE IP (as root)\n"
	"	" NAME " [OPTIONS ...] status\n"
	"	" NAME " [OPTIONS ...] revoke KEY\n"

	"Options\n"
	" -v, --verbose		verbose\n"
	" -s, --server		Server mode\n"
	" -u, --uri=URI		(server) listen to URI (default @2fad)\n"
	"			(other) connect to URI\n"
	" -o, --option=KEY[=VAL],... Add options\n"
	"			trust-script	Trust the helper script, usefull for debugging\n"
	" -t, --trust=UID	Trust remote UID as root\n"
	" -T, --timeformat=FMT	Alternate strftime format string (default %a %x %X)\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ "option", required_argument, NULL, 'o', },

	{ "uri", required_argument, NULL, 'u', },
	{ "trust", required_argument, NULL, 't', },
	{ "timeformat", required_argument, NULL, 'T', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif

static const char optstring[] = "+?Vvo:su:t:T:";

/* logging */
int max_loglevel = LOG_WARNING;
static int logtostderr = -1;

void max_loglevel_changed(void)
{
	if (!logtostderr)
		setlogmask(LOG_UPTO(max_loglevel));
}

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(progname, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];
		int skipt = max_loglevel < LOG_INFO;

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = (void *)progname, .iov_len = strlen(progname), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec+skipt, sizeof(vec)/sizeof(vec[0])-skipt);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}

#define ESTR(x) strerror(x)
#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

static unsigned long strtodelay(const char *str, char **endp, int radix)
{
	char *end;
	unsigned long result;

	if (!endp)
		endp = &end;
	result = strtoul(str, endp, radix);
	if (*endp > str) {
		switch (**endp) {
		case 'y':
			result *= 365*24*60*60;
			++*endp;
			break;
		case 'w':
			result *= 7*24*60*60;
			++*endp;
			break;
		case 'd':
			result *= 24*60*60;
			++*endp;
			break;
		case 'h':
			result *= 60*60;
			++*endp;
			break;
		case 'm':
			result *= 60;
			++*endp;
			break;
		case 's':
			result *= 1;
			++*endp;
			break;
		}
	}
	return result;
}

/* program options */
static int servermode;
static char **script;
static int trustscript;
static const char *timeformat = "%a %x %X";

static struct sockaddr_un sockname = {
	.sun_family = AF_UNIX,
	.sun_path = "\0_2fad",
};

static int trust_uid;
static const char cachefile[] = "/var/lib/2fad.state";

/* program state */
static int sock;
static volatile int sigterm;
static sigset_t saved_sigset;
static int peeruid, peergid, peerpid;

static char rbuf[16*1024];
static char tbuf[16*1024];

static void pfree(void *dat)
{
	if (dat)
		free(dat);
}

/* fix random */
unsigned int my_random(unsigned int max)
{
#ifdef HAVE_ARC4
	return arc4random_uniform(max);
#else
	static int initd;

	if (!initd) {
		int fd;
		unsigned short seedval48[3];

		fd = open("/dev/urandom", O_RDONLY | O_NONBLOCK);
		if (fd < 0) {
			mylog(LOG_WARNING, "open /dev/urandom: %s", ESTR(errno));
			srand48(time(0));
		} else {
			read(fd, seedval48, sizeof(seedval48));
			close(fd);
			seed48(seedval48);
			initd = 1;
		}
	}

	return drand48()*max;
#endif
}

static void trace_args(int loglevel, const char *msg, char **args, int nargs)
{
	int j;
	const char *quote;

	if (loglevel > max_loglevel)
		return;

	fprintf(stderr, "%s", msg);
	for (j = 0; j < nargs; ++j) {
		quote = strpbrk(args[j], " \t\n\t") ? "'" : "";
		fprintf(stderr, " %s%s%s", quote, args[j], quote);
	}
	fprintf(stderr, "\n");
	fflush(stderr);
}

/* convenience wrapper for send with SCM_CREDENTIALS */
static const char *inttostr2(int value, const char *fmt, char *buf)
{
	sprintf(buf, fmt ?: "%i", value);
	return buf;
}
static const char *inttostr(int value)
{
	static char str[48];
	sprintf(str, "%i", value);
	return str;
}

static int sendcred(const char *dat, int len, struct sockaddr_un *sa, int sockaddrlen)
{
	struct ucred *pcred;
	union {
		struct cmsghdr hdr;
		char dat[CMSG_SPACE(sizeof(struct ucred))];
	} cmsg = {
		.hdr.cmsg_len = CMSG_LEN(sizeof(struct ucred)),
		.hdr.cmsg_level = SOL_SOCKET,
		.hdr.cmsg_type = SCM_CREDENTIALS,
	};
	struct iovec iov = {
		.iov_base = (void *)dat,
		.iov_len = len,
	};
	struct msghdr msg = {
		.msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_control = &cmsg,
		.msg_controllen = cmsg.hdr.cmsg_len,
		.msg_name = sa,
		.msg_namelen = sockaddrlen,
	};
	pcred = (void *)CMSG_DATA(&cmsg.hdr);
	pcred->pid = getpid();
	pcred->uid = getuid();
	pcred->gid = getgid();
	return sendmsg(sock, &msg, 0);
}

static int sendcredvp(struct sockaddr_un *sa, int sockaddrlen, char **args, int nargs)
{
	int j, len;
	char *bufp;

	for (j = 0, bufp = tbuf; j < nargs; ++j) {
		len = strlen(args[j] ?: "");
		if (tbuf + len > tbuf+sizeof(tbuf))
			return -ERANGE;
		strcpy(bufp, args[j]);
		bufp += strlen(bufp)+1;
	}
	trace_args(LOG_DEBUG, ">", args, nargs);
	return sendcred(tbuf, bufp - tbuf, sa, sockaddrlen);
}

__attribute__((sentinel(0)))
static int sendcredlp(struct sockaddr_un *sa, int sockaddrlen, ...)
{
	va_list va;
	char *args[16];
	int nargs;

	va_start(va, sockaddrlen);
	for (nargs = 0; nargs < ARRAY_SIZE(args)-1; ++nargs) {
		args[nargs] = va_arg(va, char *);
		if (!args[nargs])
			break;
	}
	/* add NULL at the end */
	args[nargs] = NULL;
	va_end(va);
	return sendcredvp(sa, sockaddrlen, args, nargs);
}

static int recvcred(char *dat, unsigned int len, struct sockaddr_un *sockaddr, socklen_t *sockaddrlen)
{
	static char cmsgdat[CMSG_SPACE(sizeof(struct ucred))];
	struct iovec iov = {
		.iov_base = dat,
		.iov_len = len,
	};
	struct msghdr msg = {
		.msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_control = cmsgdat,
		.msg_controllen = sizeof(cmsgdat),
		.msg_name = sockaddr,
		.msg_namelen = *sockaddrlen,
	};
	struct cmsghdr *cmsg;
	int ret;

	/* recv */
	ret = recvmsg(sock, &msg, 0);
	if (ret < 0)
		return ret;
	cmsg = CMSG_FIRSTHDR(&msg);
	if (cmsg && cmsg->cmsg_level == SOL_SOCKET &&
			cmsg->cmsg_type == SCM_CREDENTIALS) {
		/* instead of accessing the data directly, and
		 * issuing a 'breaking strict aliasing' compiler
		 * warning, I decided to do this the proper way
		 */
		struct ucred uc;

		memcpy(&uc, CMSG_DATA(cmsg), sizeof(uc));
		peeruid = uc.uid;
		peergid = uc.gid;
		peerpid = uc.pid;
	} else {
		peeruid = peergid = peerpid = -1;
	}

	/* run command */
	*sockaddrlen = msg.msg_namelen;
	return ret;
}
static int recvcredvp(struct sockaddr_un *sa, socklen_t *sockaddrlen, char **pargs, int nargs)
{
	int ret, len, j;
	char *str;

	ret = len = recvcred(rbuf, sizeof(rbuf)-1, sa, sockaddrlen);
	if (ret <= 0)
		return ret;

	/* null-terminate */
	rbuf[len] = 0;

	/* parse null-seperated buffer in arguments */
	for (j = 0, str = rbuf; str < rbuf+len && j < nargs-1; ++j) {
		pargs[j] = str;
		str += strlen(str) + 1;
	}
	pargs[j] = NULL;
	trace_args(LOG_DEBUG, "<", pargs, j);
	return j;
}

/* */
struct authz {
	int uid;
	char *svc;
	char *ip;
	int random;
	int state;
		/* internal states */
		#define AUTHZ_NONE	0
		#define AUTHZ_EXPIRED	2
		#define AUTHZ_REVOKED	3
		/* deliberate gap */
		/* external states: possible in authorize response */
		#define AUTHZ_PENDING	5
		#define AUTHZ_OK	6
		#define AUTHZ_DENIED	7
		/* never: equal to denied, but it's more clear to have it a different state */
		#define AUTHZ_NEVER	8
	time_t expire;
	time_t ctime; /* create time */
	time_t atime; /* authorized time */
	time_t ltime; /* last used time */
	int pid;
};

static const char *const authz_state_str[] = {
	[AUTHZ_NONE] = "init",
	[AUTHZ_PENDING] = "wait",
	[AUTHZ_EXPIRED] = "expired",
	[AUTHZ_REVOKED] = "revoked",
	[AUTHZ_OK] = "ok",
	[AUTHZ_DENIED] = "deny",
	[AUTHZ_NEVER] = "never",
};
static int authz_state_from_str(const char *state)
{
	int j;

	for (j = 0; j < ARRAY_SIZE(authz_state_str); ++j) {
		if (!strcmp(authz_state_str[j] ?: "", state))
			return j;
	}
	return 0;
}
static int authz_is_restricted_state(int state)
{
	return state < AUTHZ_PENDING;

}

/* a linear list for struct pointer:
 * easy to sort / reorder
 * fast lookup (bsearch)
 * struct's pointers remain valid
 */
static struct authz **authz_list;
static int authz_fill;
static int authz_room;
static int (*authz_curr_cmp)(const void *, const void *);
static void authz_changed(void);

static int cmp_authz_uid_svc_ip(const void *va, const void *vb)
{
	const struct authz *const *pa = va;
	const struct authz *const *pb = vb;
	const struct authz *a = *pa;
	const struct authz *b = *pb;
	int ret;

	ret = a->uid - b->uid;
	if (ret)
		return ret;
	ret = strcmp(a->svc ?: "", b->svc ?: "");
	if (ret)
		return ret;
	ret = strcmp(a->ip ?: "", b->ip ?: "");
	if (ret)
		return ret;

	return 0;
}

static int cmp_authz_expire_desc(const void *va, const void *vb)
{
	const struct authz *const *pa = va;
	const struct authz *const *pb = vb;
	const struct authz *a = *pa;
	const struct authz *b = *pb;
	long ret;

	ret = a->expire - b->expire;
	/* make it safe to calculate a long, but return int */
	if (ret < 0)
		return +1;
	else if (ret > 0)
		return -1;
	else
		return 0;
}

static void sort_authz(int (*cmp)(const void *, const void *))
{
	if (authz_curr_cmp == cmp)
		return;
	qsort(authz_list, authz_fill, sizeof(authz_list[0]), cmp);
	authz_curr_cmp = cmp;
}

static struct authz *add_authz(int uid, const char *svc, const char *ip)
{
	struct authz *auth;

	/* create new */
	auth = malloc(sizeof(*auth));
	memset(auth, 0, sizeof(*auth));
	auth->uid = uid;
	auth->svc = strdup(svc);
	auth->ip = strdup(ip);
	auth->random = my_random(~0);
	auth->ctime = time(NULL);

	if (authz_fill >= authz_room) {
		/* grow */
		authz_room += 1024;
		authz_list = realloc(authz_list, sizeof(authz_list[0])*authz_room);
		if (!authz_list)
			mylog(LOG_ERR, "realloc authz_list %u failed: %s", authz_room, ESTR(errno));
	}
	/* append */
	authz_list[authz_fill++] = auth;
	/* mark as unsorted */
	authz_curr_cmp = 0;
	authz_changed();
	return auth;
}

static struct authz *find_authz_uid_svc_ip(int uid, const char *svc, const char *ip)
{
	struct authz needle = {
		.uid = uid,
		.svc = (char *)svc,
		.ip = (char *)ip,
	};
	struct authz *pneedle = &needle;
	struct authz **pauth;

	sort_authz(cmp_authz_uid_svc_ip);
	pauth = bsearch(&pneedle, authz_list, authz_fill, sizeof(authz_list[0]), cmp_authz_uid_svc_ip);
	if (pauth)
		return *pauth;
	return NULL;
}

static void delete_authz(struct authz *authz)
{
	/* cleanup */
	pfree(authz->svc);
	pfree(authz->ip);
	pfree(authz);
	authz_changed();
}

static void remove_authz(struct authz **pauthz)
{
	--authz_fill;
	if (authz_list+authz_fill > pauthz)
		/* move remaining items */
		memmove(pauthz, pauthz+1, sizeof(*pauthz) * (authz_list+authz_fill - pauthz));
	/* no need to sort */
	authz_changed();
}

static struct authz *authz_find_pid(int pid)
{
	struct authz *authz;
	int j;

	if (!pid)
		return NULL;
	for (j = 0; j < authz_fill; ++j) {
		authz = authz_list[j];
		if (authz->pid == pid)
			return authz;
	}
	return NULL;
}

/* clients */
struct client {
	char *peername;
	struct sockaddr_un peeraddr;
	socklen_t peeraddrlen;

	/* properties of the remote process */
	int pid;
	struct authz *authz;
};

static struct client **client_list;
static int client_fill;
static int client_room;

static void client_lost(void *dat);

static int cmp_client(const void *va, const void *vb)
{
	const struct client *const *pa = va;
	const struct client *const *pb = vb;
	const struct client *a = *pa;
	const struct client *b = *pb;
	int ret;

	ret = a->peeraddrlen - b->peeraddrlen;
	if (ret)
		return ret;
	return memcmp(&a->peeraddr, &b->peeraddr, a->peeraddrlen);
}

static void sort_client(void)
{
	qsort(client_list, client_fill, sizeof(client_list[0]), cmp_client);
}

static struct client *add_client(struct sockaddr_un *peeraddr, int peeraddrlen)
{
	struct client *client;

	/* create new */
	client = malloc(sizeof(*client));
	memset(client, 0, sizeof(*client));
	memcpy(&client->peeraddr, peeraddr, peeraddrlen);
	client->peeraddrlen = peeraddrlen;
	asprintf(&client->peername, "@%s", peeraddr->sun_path+1);

	if (client_fill >= client_room) {
		/* grow */
		client_room += 1024;
		client_list = realloc(client_list, sizeof(client_list[0])*client_room);
		if (!client_list)
			mylog(LOG_ERR, "realloc client_list %u failed: %s", client_room, ESTR(errno));
	}
	/* append */
	client_list[client_fill++] = client;
	/* let list remain sorted */
	sort_client();
	return client;
}

static struct client **find_client(struct sockaddr_un *peeraddr, int peeraddrlen)
{
	struct client needle = {
		.peeraddr = *peeraddr,
		.peeraddrlen = peeraddrlen,
	};
	struct client *pneedle = &needle;

	return bsearch(&pneedle, client_list, client_fill, sizeof(client_list[0]), cmp_client);
}

static void remove_client(struct client **pclient)
{
	struct client *client = *pclient;

	/* cleanup */
	client = *pclient;
	libt_remove_timeout(client_lost, client);
	free(client->peername);
	free(client);

	--client_fill;
	if (client_list+client_fill > pclient)
		/* move remaining items */
		memmove(pclient, pclient+1, sizeof(*pclient) * (client_list+client_fill - pclient));
	/* no need to sort */
}

/* SERVER */
static void client_lost(void *dat)
{
	struct client *client = dat;

	mylog(LOG_WARNING, "client %s lost", client->peername);
	/* if we coded well,
	 * find_client should always provide a result
	 */
	remove_client(find_client(&client->peeraddr, client->peeraddrlen));
}

static void authz_resolved(struct authz *authz)
{
	struct client *client;
	int j;

	for (j = client_fill-1; j >= 0; --j) {
		client = client_list[j];
		if (client->authz != authz)
			continue;
		sendcredlp(&client->peeraddr, client->peeraddrlen,
				"response",
				inttostr(authz->uid),
				authz->svc, authz->ip,
				authz_state_str[authz->state],
				NULL);
		remove_client(client_list+j);
	}
}

static void authz_runner(void *dat)
{
	/* cleanup expired authz */
	struct authz *authz;
	time_t now;

	/* sort list descending, and remove elements from the back
	 * until expire time in future.
	 * Due to sorting, we can exit the loop as soon as one
	 * we keep 1 item */
	time(&now);
	sort_authz(cmp_authz_expire_desc);

	for (; authz_fill > 0; --authz_fill) {
		authz = authz_list[authz_fill-1];
		if (authz->expire > now)
			break;
		mylog(LOG_NOTICE, "expire %i %s %s %s",
				authz->uid, authz->svc, authz->ip,
				authz_state_str[authz->state]);
		authz->state = AUTHZ_EXPIRED;
		authz_resolved(authz);
		delete_authz(authz);
	}

	libt_add_timeout(libt_timetointerval(60), authz_runner, dat);
}

static int request_2fa(struct authz *authz)
{
	int ret;
	struct stat st;

	/* prevent upcoming exec to fail */
	if (stat(script[0], &st) < 0) {
		mylog(LOG_WARNING, "'%s': %s", script[0], ESTR(errno));
		return -errno;
	}
	/* verify if the binary is well protected */
	if (!trustscript && st.st_mode & 0022) {
		mylog(LOG_WARNING, "%s bad mode %o", script[0], st.st_mode);
		return -ECANCELED;
	}
	if (!trustscript && st.st_uid != 0 && st.st_uid != trust_uid) {
		mylog(LOG_WARNING, "%s bad uid %i", script[0], st.st_uid);
		return -ECANCELED;
	}

	/* fork & launch helper */
	int pp[2];
	if (pipe(pp) < 0)
		mylog(LOG_ERR, "pipe() failed: %s", ESTR(errno));
	ret = fork();
	if (ret < 0)
		mylog(LOG_ERR, "fork() failed: %s", ESTR(errno));
	if (ret == 0) {
		/* child */
		/* close pipe write end */
		close(pp[1]);
		dup2(pp[0], STDIN_FILENO);
		close(pp[0]);
		/* restore signal mask */
		sigprocmask(SIG_SETMASK, &saved_sigset, NULL);
		execvp(script[0], script);
		mylog(LOG_ERR, "execvp %s: %s", script[0], ESTR(errno));
		exit(1);
	}
	/* parent */
	authz->pid = ret;
	snprintf(tbuf, sizeof(tbuf), "%i\n%s\n%s\n%08x\n",
			authz->uid, authz->svc, authz->ip, authz->random);
	ret = write(pp[1], tbuf, strlen(tbuf));
	close(pp[0]);
	close(pp[1]);
	if (ret < 0) {
		mylog(LOG_WARNING, "write pipe, %s", ESTR(errno));
		return -errno;
	}
	authz->state = AUTHZ_PENDING;
	authz->expire = time(NULL) + 300;
	authz_changed();
	return 0;
}

static int server_load_auth_table(const char *file)
{
	FILE *fp;
	char *line = NULL;
	size_t linesize = 0;
	int ret;
	int uid;
	char *svc, *ip;
	struct stat st;
	struct authz *authz;
	char *tok, *val;

	fp = fopen(file, "r");
	if (!fp) {
		if (errno == ENOENT)
			mylog(LOG_WARNING, "no cache file");
		else
			mylog(LOG_WARNING, "fopen %s r: %s", file, ESTR(errno));
		return -1;
	}
	fstat(fileno(fp), &st);
	if (st.st_mode & 0077) {
		mylog(LOG_WARNING, "%s bad mode %o", file, st.st_mode);
		goto bad;
	}
	if (st.st_uid != 0 && st.st_uid != trust_uid) {
		mylog(LOG_WARNING, "%s bad uid %i", file, st.st_uid);
		goto bad;
	}
	mylog(LOG_INFO, "load %s", file);
	for (;;) {
		ret = getline(&line, &linesize, fp);
		if (!ret)
			break;
		if (ret < 0 && feof(fp))
			break;
		if (ret < 0) {
			mylog(LOG_WARNING, "getline %s: %s", file, ESTR(errno));
			break;
		}
		line[ret] = 0;
		uid = strtoul(strtok(line, " \t\r\n") ?: "", NULL, 0);
		svc = strtok(NULL, " \t\r\n");
		ip = strtok(NULL, " \t\r\n");
		authz = add_authz(uid, svc, ip);

		authz->random = strtoul(strtok(NULL, " \t\r\n") ?: "", NULL, 16);
		authz->state = authz_state_from_str(strtok(NULL, " \t\r\n") ?: "ok");
		for (;;) {
			tok = strtok(NULL, " \t\r\n");
			if (!tok)
				break;
			val = strchr(tok ?: "", '=');
			if (val) {
				*val++ = 0;
			}
			if (!strcmp(tok, "ex"))
				authz->expire = strtoul(val ?: "", NULL, 0);
			else if (!strcmp(tok, "ct"))
				authz->ctime = strtoul(val ?: "", NULL, 0);
			else if (!strcmp(tok, "at"))
				authz->atime = strtoul(val ?: "", NULL, 0);
			else if (!strcmp(tok, "lt"))
				authz->ltime = strtoul(val ?: "", NULL, 0);
			else
				mylog(LOG_WARNING, "unknown key '%s' in state", tok);
		}
	}
	pfree(line);
	if (0) {
		/* file was considered bad, move away,
		 * avoid overwriting because we didn't use it
		 */
		char *badfilename;
bad:
		asprintf(&badfilename, "%s.bad", file);
		if (rename(file, badfilename) < 0)
			mylog(LOG_WARNING, "mv %s %s: %s", file, badfilename, ESTR(errno));
		free(badfilename);
	}
	fclose(fp);
	return authz_fill;
}

static int server_save_auth_table(const char *file)
{
	char *tmpfile;
	FILE *fp;
	struct authz *authz;
	int result = -1;
	int j;

	asprintf(&tmpfile, "%s-%i", file, getpid());
	fp = fopen(tmpfile, "w");
	if (!fp) {
		mylog(LOG_WARNING, "fopen %s w: %s", tmpfile, ESTR(errno));
		goto fail_open;
	}
	mylog(LOG_INFO, "save %s", file);
	/* chmod readonly */
	fchmod(fileno(fp), 0600);
	for (j = 0; j < authz_fill; ++j) {
		authz = authz_list[j];

		fprintf(fp, "%u %s %s %08x %s ex=%llu ct=%llu at=%llu lt=%llu\n"
				, authz->uid, authz->svc, authz->ip, authz->random
				, authz_state_str[authz->state]
				, (long long)authz->expire
				, (long long)authz->ctime
				, (long long)authz->atime
				, (long long)authz->ltime
				);
	}
	fclose(fp);
	if (rename(tmpfile, file) < 0) {
		mylog(LOG_WARNING, "rename %s %s: %s", tmpfile, file, ESTR(errno));
		if (unlink(tmpfile) < 0)
			mylog(LOG_WARNING, "unlink %s: %s", tmpfile, ESTR(errno));
		goto fail_rename;
	}
	result = authz_fill;

fail_rename:
fail_open:
	free(tmpfile);
	return result;
}

static int authz_save_scheduled;
static void authz_do_save(void *dat)
{
	server_save_auth_table(cachefile);
	authz_save_scheduled = 0;
}
static void authz_changed(void)
{
	if (!authz_save_scheduled)
		libt_add_timeout(60, authz_do_save, NULL);
	authz_save_scheduled = 1;
}

static void on_socket_server(int sock, void *dat)
{
	char *args[16];
	int nargs;
	int j;
	int uid;
	int random_value;
	time_t delay;
	static struct sockaddr_un peeraddr;
	socklen_t peeraddrlen;
	struct client *client, **pclient;
	struct authz *authz;

	peeraddrlen = sizeof(peeraddr);
	nargs = recvcredvp(&peeraddr, &peeraddrlen, args, ARRAY_SIZE(args));
	if (!nargs)
		return;

	pclient = find_client(&peeraddr, peeraddrlen);
	client = pclient ? *pclient : NULL;
	if (client)
		libt_add_timeout(3, client_lost, client);

	if (!strcmp(args[0], "keepalive")) {
		/* nothing special */
		sendcredlp(&peeraddr, peeraddrlen, "keepalive", NULL);

	} else if (!strcmp(args[0], "ls")) {
		int done = 0, eperm = 0;
		int todo = 0;
			#define TODO_UID	(1 << 1)
			#define TODO_SVC	(1 << 2)
			#define TODO_IP		(1 << 3)
			#define TODO_RANDOM	(1 << 4)
		char *svc = NULL, *ip = NULL;
		char *endp;

		uid = random_value = 0;
		if (nargs > 1) {
			uid = strtoul(args[1], &endp, 0);
			if (endp > args[1])
				todo |= 1 << 1;
		}
		if (nargs > 2) {
			svc = args[2];
			if (strcmp(svc, "-"))
				todo |= 1 << 2;
		}
		if (nargs > 3) {
			ip = args[3];
			if (strcmp(ip, "-"))
				todo |= 1 << 3;
		}
		if (nargs > 4) {
			random_value = strtoul(args[4], &endp, 16);
			if (endp > args[4])
				todo |= 1 << 4;
		}

		char uidstr[16], rand_str[16], expire_str[32];
		char ct_str[32], at_str[32], lt_str[32];

		for (j = 0; j < authz_fill; ++j) {
			authz = authz_list[j];
			if ((todo & TODO_UID) && uid != authz->uid)
				continue;
			if ((todo & TODO_SVC) && strcmp(svc ?: "", authz->svc))
				continue;
			if ((todo & TODO_IP) && strcmp(ip ?: "", authz->ip))
				continue;
			if ((todo & TODO_RANDOM) && random_value != authz->random)
				continue;
			if (peeruid && peeruid != trust_uid && peeruid != authz->uid) {
				++eperm;
				continue;
			}
			++done;
			sendcredlp(&peeraddr, peeraddrlen, "ls",
					inttostr2(authz->uid, "%u", uidstr),
					authz->svc, authz->ip,
					inttostr2(authz->random, "%08x", rand_str),
					authz_state_str[authz->state],
					inttostr2(authz->expire, "ex=%lu", expire_str),
					inttostr2(authz->ctime, "ct=%lu", ct_str),
					inttostr2(authz->atime, "at=%lu", at_str),
					inttostr2(authz->ltime, "lt=%lu", lt_str),
					NULL);
		}

		if (!done && eperm)
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EPERM), strerror(EPERM), NULL);
		else
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(done), "ok", NULL);

	} else if (!strcmp(args[0], "request")) {
		/* find authz */
		if (nargs < 4) {
			/* require uid, service, ip */
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL),
					"4 arguments required for 'request'", NULL);
			return;
		}
		uid = strtoul(args[1], NULL, 10);
		if (peeruid && peeruid != trust_uid && peeruid != uid) {
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EPERM),
					strerror(EPERM), NULL);
			return;
		}
		authz = find_authz_uid_svc_ip(uid, args[2], args[3]);
		if (!authz)
			authz = add_authz(uid, args[2], args[3]);
		if (client)
			/* assign authz if client existed already */
			client->authz = authz;
		authz->ltime = time(NULL);

		/* if we know the authz result already,
		 * it's not strictly necessary to create a client */
		if (authz->state >= AUTHZ_OK) {
			sendcredlp(&peeraddr, peeraddrlen,
					"response",
					inttostr(authz->uid),
					authz->svc, authz->ip,
					authz_state_str[authz->state],
					NULL);
			if (client)
				remove_client(pclient);
			return;
		}

		/* create client, we must remember the request */
		if (!client)
			client = add_client(&peeraddr, peeraddrlen);
		client->pid = peerpid;
		client->authz = authz;

		if (authz->state == AUTHZ_NONE)
			request_2fa(authz);
		sendcredlp(&client->peeraddr, client->peeraddrlen,
				"response",
				inttostr(authz->uid),
				authz->svc, authz->ip,
				authz_state_str[authz->state],
				NULL);

	} else if (!strcmp(args[0], "revoke")) {
		/* 'revoke' UID RANDOM */
		if (nargs != 3) {
			/* requires random */
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL),
					"2 arguments required for 'revoke'", NULL);
			return;
		}
		uid = strtoul(args[1], NULL, 10);
		random_value = strtoul(args[2], NULL, 16);

		if (peeruid && peeruid != trust_uid && peeruid != uid) {
			sendcredlp(&peeraddr, peeraddrlen, "done",
					inttostr(-EPERM), strerror(EPERM), NULL);
			return;
		}
		/* look for authz */
		for (j = 0; j < authz_fill; ++j) {
			authz = authz_list[j];
			if (authz->uid == uid && authz->random == random_value) {
				mylog(LOG_NOTICE, "revoke %i %s %s",
						authz->uid, authz->svc, authz->ip);
				authz->state = AUTHZ_REVOKED;
				remove_authz(authz_list+j);
				authz_resolved(authz);
				delete_authz(authz);
				sendcredlp(&peeraddr, peeraddrlen, "done", "0", "ok", NULL);
				return;
			}
		}
		sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-ENOENT), strerror(ENOENT), NULL);

	} else if (!strcmp(args[0], "authorize")) {
		/* authz results received */
		/* 'authorize' UID SERVICE IP RANDOM RESULT EXPIRE */
		int state;

		if (nargs != 7) {
			mylog(LOG_WARNING, "malformed 'authorize' received");
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL),
					"bad number of arguments", NULL);
			return;
		}
		if (peeruid && peeruid != trust_uid) {
			mylog(LOG_WARNING, "'authorize' from untrusted uid %i", peeruid);
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EPERM), strerror(EPERM), NULL);
			return;
		}
		state = authz_state_from_str(args[5]);
		if (authz_is_restricted_state(state)) {
			mylog(LOG_WARNING, "'authorize' bad state '%s'", args[5]);
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL), "bad state", NULL);
			return;
		}

		authz = find_authz_uid_svc_ip(strtoul(args[1], NULL, 10), args[2], args[3]);
		if (!authz || authz->state != AUTHZ_PENDING) {
			mylog(LOG_WARNING, "'authorize' received, but not waiting");
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL),
					"not waiting", NULL);
			return;
		}
		random_value = strtoul(args[4], NULL, 16);
		if (random_value != authz->random) {
			mylog(LOG_WARNING, "'authorize' with bad key received");
			sendcredlp(&peeraddr, peeraddrlen, "done", inttostr(-EINVAL),
					"bad key", NULL);
			return;
		}
		authz->state = state;
		delay = strtodelay(args[6], NULL, 0);
		authz->expire = time(NULL) + delay;
		authz->atime = time(NULL);
		authz_changed();

		mylog(LOG_NOTICE, "authorize %i %s %s %s %llu",
				authz->uid, authz->svc, authz->ip,
				authz_state_str[authz->state],
				(long long)delay);
		sendcredlp(&peeraddr, peeraddrlen, "done", "0", "ok", NULL);

		authz_resolved(authz);
	}
}

/* CLIENT */
static int client_result;
static void keepalive(void *dat)
{
	sendcredlp(NULL, 0, "keepalive", NULL);
	libt_repeat_timeout(1, keepalive, dat);
}

static void server_lost(void *dat)
{
	mylog(LOG_WARNING, "server timeout");
	/* terminate with failure */
	sigterm = 1;
	client_result = 1;
}

static char *uid_to_name(int uid)
{
	struct passwd *pw;

	pw = getpwuid(uid);
	if (pw)
		return pw->pw_name;
	return NULL;
}
static const char *name_to_uid(const char *name)
{
	char *endp;
	struct passwd *pw;

	strtoul(name, &endp, 0);
	if (endp > name && !*endp)
		return name;

	pw = getpwnam(name);
	if (pw) {
		char uidbuf[16];
		return inttostr2(pw->pw_uid, "%u", uidbuf);
	}
	return name;
}

static char *time_to_str(time_t t, const char *prefix)
{
	static char *str;
	static char tstr[64];
	const char *quote;

	if (!t)
		strcpy(tstr, "0");
	else
		strftime(tstr, sizeof(tstr), timeformat, localtime(&t));

	quote = strpbrk(tstr, " \t\n\t") ? "'" : "";
	pfree(str);
	asprintf(&str, "%s=%s%s%s", prefix, quote, tstr, quote);
	return str;
}

static void on_socket_client(int sock, void *dat)
{
	char *args[16];
	int nargs;
	int j;

	static struct sockaddr_un peeraddr;
	socklen_t peeraddrlen;

	peeraddrlen = sizeof(peeraddr);
	nargs = recvcredvp(&peeraddr, &peeraddrlen, args, ARRAY_SIZE(args));
	if (!nargs)
		return;

	libt_add_timeout(2, server_lost, NULL);

	if (peeruid && peeruid != trust_uid) {
		mylog(LOG_WARNING, "untrusted peer uid %i", peeruid);
		return;
	}

	if (!strcmp(args[0], "keepalive")) {
		/* nothing special */

	} else if (!strcmp(args[0], "done")) {
		client_result = strtol(args[1] ?: "1", NULL, 0) < 0;
		sigterm = 1;
		if (client_result && nargs > 2)
			mylog(LOG_WARNING, "server said: %s", args[2]);

	} else if (!strcmp(args[0], "response")) {
		/* 'response' UID SVC IP 'ok' */
		int state;

		if (nargs <= 4)
			mylog(LOG_WARNING, "bad server response");
		state = authz_state_from_str(args[4]);
		if (state != AUTHZ_OK)
			mylog(LOG_WARNING, "response %s", authz_state_str[state]);

		if (state == AUTHZ_PENDING) {
			libt_add_timeout(1, keepalive, NULL);
			return;
		}
		client_result = state != AUTHZ_OK;
		sigterm = 1;

	} else if (!strcmp(args[0], "ls")) {
		char *str;

		for (j = 1; j < nargs; ++j) {
			str = args[j];
			if (j == 1) {
				/* uid: print uid, and resolve username */
				fprintf(stdout, "%s", str);
				str = uid_to_name(strtoul(str, NULL, 0)) ?: str;
			}

			else if (!strncmp(str, "ex=", 3))
				str = time_to_str(strtoul(str+3, NULL, 10), "ex");
			else if (!strncmp(str, "ct=", 3))
				str = time_to_str(strtoul(str+3, NULL, 10), "ct");
			else if (!strncmp(str, "at=", 3))
				str = time_to_str(strtoul(str+3, NULL, 10), "at");
			else if (!strncmp(str, "lt=", 3))
				str = time_to_str(strtoul(str+3, NULL, 10), "lt");

			fprintf(stdout, " %s", str);
		}
		fprintf(stdout, "\n");
		fflush(stdout);

	} else {
		mylog(LOG_WARNING, "unexpected '%s'", args[0]);
		client_result = 1;
		sigterm = 1;
	}
}

static void on_signalfd(int fd, void *dat)
{
	int ret, status, result;
	struct signalfd_siginfo info;
	struct authz *authz;

	/* signals */
	ret = read(fd, &info, sizeof(info));
	if (ret < 0 && errno == EAGAIN)
		return;
	if (ret < 0)
		mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
	switch (info.ssi_signo) {
	case SIGINT:
	case SIGTERM:
		sigterm = 1;
		break;
	case SIGHUP:
		break;
	case SIGCHLD:
		for (;;) {
			ret = waitpid(-1, &status, WNOHANG);
			if (ret < 0 && errno != ECHILD)
				mylog(LOG_WARNING, "waitpid: %s", ESTR(errno));
			if (ret <= 0)
				break;
			if (WIFEXITED(status))
				result = WEXITSTATUS(status);
			else if (WIFSIGNALED(status))
				result = 128 + WTERMSIG(status);
			else
				result = 256;
			authz = authz_find_pid(ret);
			if (authz && result) {
				mylog(LOG_WARNING, "helper %i %s %s failed: %i",
						authz->uid, authz->svc, authz->ip,
						result);
				authz->state = AUTHZ_NONE;
				authz_resolved(authz);
			}
			if (authz)
				authz->pid = 0;
		}
		break;
	}
}

/* main process */
int main(int argc, char *argv[])
{
	int ret, fd, opt;
	char *str;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
		fprintf(stderr, "run '%s -?' for help\n", argv[0]);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'o':
		for (optarg = strtok(optarg, ","); optarg; optarg = strtok(NULL, ",")) {
			if (!strcmp(optarg, "trust-script")) {
				trustscript = 1;
			} else {
				fprintf(stderr, "unknown option -o %s\n", optarg);
				fprintf(stderr, "run '%s -?' for help\n", argv[0]);
				exit(1);
			}
		}
		break;
	case 's':
		servermode = 1;
		break;
	case 'u':
		memset(sockname.sun_path, 0, sizeof(sockname.sun_path));
		strncpy(sockname.sun_path, optarg, sizeof(sockname.sun_path));
		if (*optarg == '@')
			sockname.sun_path[0] = 0;
		break;
	case 't':
		trust_uid = strtoul(optarg, NULL, 0);
		break;
	case 'T':
		timeformat = optarg;
		break;
	}

	if (optind >= argc) {
		fputs(help_msg, stderr);
		exit(1);
	}

	/* setup signals */
	mylog(LOG_INFO, "open signalfd");
	sigset_t set;
	sigfillset(&set);
	//sigdelset(&set, SIGINT);
	sigprocmask(SIG_BLOCK, &set, &saved_sigset);
	fd = ret = signalfd(-1, &set, SFD_CLOEXEC | SFD_NONBLOCK);
	if (ret < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(fd, on_signalfd, NULL);

	if (servermode) {
		script = argv+optind;

		/* open server socket */
		sock = socket(PF_UNIX, SOCK_DGRAM | SOCK_CLOEXEC | SOCK_NONBLOCK, 0);
		if (sock < 0)
			mylog(LOG_ERR, "socket(unix, ...) failed: %s", ESTR(errno));

		ret = bind(sock, (void *)&sockname, sizeof(sockname));
		if (ret < 0)
			mylog(LOG_ERR, "bind(@%s) failed: %s", sockname.sun_path+1, ESTR(errno));

		ret = 1;
		ret = setsockopt(sock, SOL_SOCKET, SO_PASSCRED, &ret, sizeof(ret));
		if (ret < 0)
			mylog(LOG_ERR, "setsockopt SO_PASSCRED failed: %s", ESTR(errno));
		libe_add_fd(sock, on_socket_server, NULL);

		/* initial load */
		server_load_auth_table(cachefile);
		/* flush immediately */
		authz_runner(NULL);
	} else {
		setlocale(LC_ALL, "");
		/* force logging to stderr */
		logtostderr = 1;
		/* client */
		ret = sock = socket(PF_UNIX, SOCK_DGRAM, 0);
		if (ret < 0)
			mylog(LOG_ERR, "socket(unix, ...) failed: %s", ESTR(errno));
		/* connect to server */
		ret = connect(sock, (void *)&sockname, sizeof(sockname));
		if (ret < 0)
			mylog(LOG_ERR, "connect(@%s) failed: %s", sockname.sun_path+1, ESTR(errno));

		str = sockname.sun_path+1;
		str += strlen(str);
		sprintf(str, "-%i", getpid());
		ret = bind(sock, (void *)&sockname, sizeof(sockname));
		if (ret < 0)
			mylog(LOG_ERR, "bind(@%s) failed: %s", sockname.sun_path+1, ESTR(errno));

		ret = 1;
		ret = setsockopt(sock, SOL_SOCKET, SO_PASSCRED, &ret, sizeof(ret));
		if (ret < 0)
			mylog(LOG_ERR, "setsockopt SO_PASSCRED failed: %s", ESTR(errno));
		libe_add_fd(sock, on_socket_client, NULL);

		if (!strcmp(argv[optind], "request")) {
			/* must prepend user */
			char uid_str[16];
			char *args[5] = {};

			if ((argc - optind) == 3) {
				/* 'request' SVC IP */
				sprintf(uid_str, "%i", getuid());
				args[0] = argv[optind];
				args[1] = uid_str;
				args[2] = argv[optind+1];
				args[3] = argv[optind+2];
			} else {
				args[0] = argv[optind];
				args[1] = argv[optind+1];
				args[2] = argv[optind+2];
				args[3] = argv[optind+3];
			}
			args[1] = (char *)name_to_uid(args[1]);
			ret = sendcredvp(NULL, 0, args, 4);
			if (ret < 0)
				mylog(LOG_ERR, "send ...: %s", ESTR(errno));
		} else {
			ret = sendcredvp(NULL, 0, argv+optind, argc - optind);
			if (ret < 0)
				mylog(LOG_ERR, "send ...: %s", ESTR(errno));
		}

		libt_add_timeout(2, server_lost, NULL);
	}

	/* happy working ... */
	mylog(LOG_INFO, "ready");
	for (; !sigterm;) {
		ret = libe_wait(libt_get_waittime());
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			/* no test for EINTR using signalfd ... */
			mylog(LOG_ERR, "libe_wait: %s", ESTR(errno));
		libe_flush();
		libt_flush();
	}
	mylog(LOG_INFO, "terminate");
	if (servermode) {
		server_save_auth_table(cachefile);
	}
	return servermode ? 0 : client_result;;
}
