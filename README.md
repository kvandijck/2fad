# 2-factor-daemon

## overview

`2fad` provides a way to implement 2-factor authentication.

## server usage

TODO

### openssh
in `/etc/ssh/sshd_config`

```
Match Group users
        ForceCommand /etc/ssh/connect.sh
```

### sambe
in `/etc/samba/smb.conf`

```
	preexec = 2fad request file %I
```

## child usage

TODO

## direct use

